package de.fearnixx.t3.admintools;

import de.fearnixx.jeak.event.IQueryEvent;
import de.fearnixx.jeak.event.bot.IBotStateEvent;
import de.fearnixx.jeak.reflect.*;
import de.fearnixx.jeak.service.controller.IRestControllerService;
import de.fearnixx.jeak.service.event.IEventService;
import de.fearnixx.jeak.service.locale.ILocalizationUnit;
import de.fearnixx.jeak.service.task.ITaskService;
import de.fearnixx.jeak.teamspeak.data.IClient;
import de.fearnixx.t3.admintools.controllers.ServerInfoController;
import de.fearnixx.t3.admintools.tasks.StaleWatchTask;
import de.mlessmann.confort.LoaderFactory;
import de.mlessmann.confort.api.IConfig;
import de.mlessmann.confort.api.IConfigNode;
import de.mlessmann.confort.api.IValueHolder;
import de.mlessmann.confort.api.except.ParseException;
import de.mlessmann.confort.api.lang.IConfigLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@JeakBotPlugin(id = "admintools", version = "1.0.0")
public class AdminTools {

    private static final String DEFAULT_CONFIG_PATH = "/admintools/defaultConfig.json";
    private static final Logger logger = LoggerFactory.getLogger(AdminTools.class);

    @Inject
    @Config(id = "admintools")
    private IConfig myConfigRef;
    private IConfigNode myConfig;

    @Inject
    private IEventService eventService;

    @Inject
    private IInjectionService injectService;

    @Inject
    private IRestControllerService controllerService;

    @Inject
    private ITaskService taskService;

    @Inject
    @LocaleUnit(value = "admintools", defaultResource = "admintools/lang.json")
    private ILocalizationUnit localization;

    private List<String> toGreet = new ArrayList<>();

    private boolean loadConfig() {
        try {
            myConfigRef.load();
        } catch (ParseException | IOException e) {
            logger.error("Failed to load configuration!", e);
            return false;
        }
        myConfig = myConfigRef.getRoot();

        if (myConfig.isVirtual()) {
            try (InputStream in = AdminTools.class.getResourceAsStream(DEFAULT_CONFIG_PATH)) {
                if (in == null) {
                    throw new FileNotFoundException("Failed to find resource: " + DEFAULT_CONFIG_PATH);
                }
                IConfigLoader loader = LoaderFactory.getLoader("application/json");
                InputStreamReader reader = new InputStreamReader(in, StandardCharsets.UTF_8);
                URI sourceLocator = new URI("classpath:" + DEFAULT_CONFIG_PATH);
                myConfigRef.setRoot(loader.parse(reader, sourceLocator));
                myConfig = myConfigRef.getRoot();
                saveConfig();

            } catch (IOException | ParseException | URISyntaxException e) {
                logger.error("Failed to load default configuration!", e);
            }
            return false;
        }

        myConfig.getNode("greet")
                .optList()
                .orElseGet(Collections::emptyList)
                .stream()
                .filter(IConfigNode::isPrimitive)
                .map(IValueHolder::asString)
                .forEach(toGreet::add);

        return true;
    }

    private boolean saveConfig() {
        try {
            myConfigRef.save();
        } catch (IOException e) {
            logger.warn("Failed to save configuration!", e);
            return false;
        }
        return true;
    }

    @Listener
    public void onInitialize(IBotStateEvent.IInitializeEvent event) {

        // Config loaded successfully ?
        if (!loadConfig()) {
            logger.warn("Config was not loaded successfully. Canceling startup.");
            event.cancel();
        }

        StaleWatchTask watchTask = new StaleWatchTask();
        injectService.injectInto(watchTask);
        eventService.registerListener(watchTask);
        taskService.scheduleTask(watchTask.getTaskDefinition());
        ServerInfoController infoController = new ServerInfoController(watchTask);
        injectService.injectInto(infoController);
        controllerService.registerController(ServerInfoController.class, infoController);
    }

    @Listener
    public void onClientConnected(IQueryEvent.INotification.IClientEnter event) {
        IClient client = event.getTarget();
        if (toGreet.stream()
                .anyMatch(name -> client.getNickName().contains(name))) {

            String greeting = localization.getContext(client.getCountryCode())
                    .optMessage("greet.message")
                    .orElse("This is a greeting.");

            event.getConnection().sendRequest(client.sendMessage(greeting));
        }
    }
}
