package de.fearnixx.t3.admintools.controllers;

import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.reflect.RequestMapping;
import de.fearnixx.jeak.reflect.RestController;
import de.fearnixx.jeak.service.controller.IResponseEntity;
import de.fearnixx.jeak.service.controller.RequestMethod;
import de.fearnixx.jeak.service.controller.ResponseEntity;
import de.fearnixx.jeak.teamspeak.cache.IDataCache;
import de.fearnixx.t3.admintools.controllers.responses.ChannelEntity;
import de.fearnixx.t3.admintools.controllers.responses.ClientEntity;
import de.fearnixx.t3.admintools.controllers.responses.ClientInfoEntity;
import de.fearnixx.t3.admintools.tasks.StaleWatchTask;

import java.util.List;
import java.util.stream.Collectors;

@RestController(pluginId = "admintools", endpoint = "/serverInfo")
public class ServerInfoController {

    @Inject
    private IDataCache dataCache;
    private StaleWatchTask watchTask;

    public ServerInfoController(StaleWatchTask watchTask) {
        this.watchTask = watchTask;
    }

    @RequestMapping(endpoint = "/channels", method = RequestMethod.GET, isSecured = false)
    public IResponseEntity<List<ChannelEntity>> getChannels() {
        List<ChannelEntity> channels = dataCache.getChannels()
                .stream()
                .map(ChannelEntity::fromData)
                .collect(Collectors.toList());
        return new ResponseEntity<>(channels);
    }

    @RequestMapping(endpoint = "/clients", method = RequestMethod.GET, isSecured = false)
    public IResponseEntity<ClientInfoEntity> getClients() {
        List<ClientEntity> activeClients = dataCache.getClients()
                .stream()
                .map(ClientEntity::fromData)
                .collect(Collectors.toList());
        List<ClientEntity> staleClients = watchTask.getStaleClients()
                .stream()
                .map(ClientEntity::fromData)
                .collect(Collectors.toList());
        return new ResponseEntity<>(new ClientInfoEntity(activeClients, staleClients));
    }
}
