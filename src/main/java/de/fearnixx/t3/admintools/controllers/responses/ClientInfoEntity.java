package de.fearnixx.t3.admintools.controllers.responses;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ClientInfoEntity {

    public ClientInfoEntity(List<ClientEntity> activeClients, List<ClientEntity> recentlyDisconnected) {
        this.activeClients = activeClients;
        this.recentlyDisconnected = recentlyDisconnected;
    }

    @JsonProperty
    private List<ClientEntity> activeClients;

    @JsonProperty
    private List<ClientEntity> recentlyDisconnected;
}
