package de.fearnixx.t3.admintools.tasks;

import de.fearnixx.jeak.event.IQueryEvent;
import de.fearnixx.jeak.reflect.Listener;
import de.fearnixx.jeak.service.task.ITask;
import de.fearnixx.jeak.teamspeak.data.IClient;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class StaleWatchTask implements Runnable {

    public ITask getTaskDefinition() {
        return ITask.builder()
                .name("StaleClients-Update")
                .interval(2, TimeUnit.MINUTES)
                .runnable(this)
                .build();
    }

    private final List<CacheEntry> staleClients = new CopyOnWriteArrayList<>();

    @Override
    public synchronized void run() {
        final LocalDateTime current = LocalDateTime.now();
        staleClients.removeIf(entry -> entry.expiry.isBefore(current));
    }

    @Listener(order = Listener.Orders.LATER)
    public synchronized void onClientEnter(IQueryEvent.INotification.IClientEnter event) {
        IClient target = event.getTarget();
        staleClients.removeIf(entry -> entry.clientObject.getClientUniqueID().equals(target.getClientUniqueID()));
    }

    @Listener(order = Listener.Orders.LATER)
    public synchronized void onClientLeave(IQueryEvent.INotification.IClientLeave event) {
        IClient target = event.getTarget();
        CacheEntry entry = new CacheEntry();
        entry.clientObject = target;
        entry.expiry = LocalDateTime.now().plusMinutes(5);
        staleClients.add(entry);
    }

    public synchronized List<IClient> getStaleClients() {
        return staleClients.stream()
                .map(entry -> entry.clientObject)
                .collect(Collectors.toList());
    }

    private static class CacheEntry {
        private IClient clientObject;
        private LocalDateTime expiry;
    }
}
